#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1root.sh' encrypted at Sun Nov 24 07:17:33 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SY\��  ����}�����_/����g���wW����嵀 3l��(�L5C�S&���M 6�i��       mDLL�	�14i��0�#Fi� �  �4` H�ҘP����hC@ �h1�4ѦA��S!&`LMb0 L4�р�d�&F  0MJd M !OD�(�M2f�#CO)�=@6��=Cj C@�d��iܹ���*����y���|��hfuSHGDS�4�Ԁ��S1��`1�4(M�f��D6(O�DT�5Ѕ)�ˎ�2��җ
�s���֝�{��y�c�\� =C� �<�ȗWw���A���G	��uB"	EI9�uH��G#�JH>�,8�m���=�5�<�`ZQIo�ʍ� �Y�	�X�k���`A#G&�T*� ��b�+��B�"��zŦ�����-5OK[z�[��)a��%�2`*���F�|��r4�@�@n>c��NI����,!m�1�kz>�h[�.��}�fq_g��FxX����A�h�J�3� �⼤��fm@a�M��r9H�6�HxG@�^�o�BV�P�Q)G�a�4v�A�D��ʠ�1D(����i�W�yo��V={�_C>��\� yB�[CC���I%�,�=b�����r�L;2�LPH�ґ�`�X��S�q�cWpbS���Yw*a��#G����O��Q�
�va�\�px�d�|�������׈�,�b'@@k��5ȃD"b����Q2Ҩ;IJ�2���\`k�V5a/&<��������Ai:�Q(��8���c�������%A�P,!J�m�b��&d�4&1,+J��m�gK��2@L% @��G8**`�a�i��Џ��Jp��T�9�iL���/�y�va�e��2����F�m�H���|-9���¶oG�_DM� �bB��e�[���$�U��/jJ��g�P�?����d���Y��Ҡ�M5;?����+�ʛ��������wiq�1��e�4]���)�ʚT�,m�б�m���cĘ��3�QȩAV5&8q)�D��R��/d�Œ21�It���8�Ǚ�5�^Kh�2�T�}06�&�[�n���#�9e�~�Q�a��t���P�j�@�I���H�
��� 